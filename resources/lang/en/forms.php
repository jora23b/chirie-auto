<?php

return [
    'email' => 'Email',
    'name' => 'Name',
    'phone' => 'Phone',
    'message' => 'Message',
];
