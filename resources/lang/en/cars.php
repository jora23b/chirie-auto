<?php

return [
    'specifications' => [
        'fuels' => [
            'diesel' => 'Diesel',
            'gas-petrol' => 'Gas / Petrol (methane)',
            'gas-gasoline' => 'Gas / Gasoline (propane)',
            'hybrid' => 'Hybrid',
            'electricity' => 'Electricity',
        ],
        'transmissions' => [
            'automatic' => 'Automatic',
            'manual' => 'Manual',
        ],
    ],
];

/*
 *
 * DB_CONNECTION=mysql
DB_HOST=192.168.10.12
DB_PORT=3306
DB_DATABASE=auto
DB_USERNAME=homestead
DB_PASSWORD=secret
 * */