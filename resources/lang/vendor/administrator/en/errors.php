<?php

return [
    'unauthorized' => 'This action is unauthorized.',
    'login_failed' => 'Login attempt failed.',
    'permission_delete_page' => 'No permission delete this page.',
];
