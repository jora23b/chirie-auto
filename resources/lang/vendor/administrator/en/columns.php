<?php

/*
|--------------------------------------------------------------------------
| Columns translations.
|--------------------------------------------------------------------------
|
| This file serves as a main translation place for all your columns.
| Instead of defining FormElement::update() method, just add your
| column here. Columns having global name can be defined also.
*/

return [
//    'some_module' => [
//        'desc' => 'Hint'
//    ],

    'global' => [
         'images' => 'Images',
    ],
    'menu' => [
        'locations' => [
            'top' => 'Top',
            'bottom' => 'Bottom',
        ],
    ],
    'cars' => [
        'types' => [
           'sale' => 'Sale',
           'rent' => 'Rent',
        ],
    ],
    'sections' => [
        'general' => 'General',
        'meta' => 'Meta',
        'specifications' => 'Specifications',
        'images' => 'Images',
    ],
];