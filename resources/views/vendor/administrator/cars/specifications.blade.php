<div class="translatable-block">
    <div class="tab-content">
        @foreach(\localizer\locales() as $locale)
            <div class="tab-pane{{ $locale->id() == \localizer\locale()->id() ? ' active' : ''}}" id="specification_{{ $id }}_{{ $locale->id() }}">
                <div class="translatable" data-locale="{{ $locale->id() }}">
                    <input style="width: 100%;" class="form-control" name="translatable[{{ $locale->id() }}][specifications_{{ $id }}]" type="text"
                           value="{{ isset($values[$locale->id()]) ? $values[$locale->id()] : '' }}">
                </div>
            </div>
        @endforeach
    </div>
    <ul class="nav nav-tabs nav-translatable">
        @foreach(\localizer\locales() as $locale)
            <li class="{{ $locale->id() == \localizer\locale()->id() ? 'active' : ''}}">
                <a href="#specification_{{ $id }}_{{ $locale->id() }}" data-toggle="tab" aria-expanded="false">
                    <strong>{{ $locale->iso6391() }}</strong>
                </a>
            </li>
        @endforeach
    </ul>
</div>