<div class="panel mt50" id="spy2">
    <div class="panel-body">
        <div class="body gallery-page">
            <div id="mix-container" class="grid" style="margin-top: 10px;">
                <span class="hide" data-url="" id="url_remove_image"></span>
                @foreach($row->images as $image)
                    <div class="mix label2 folder2" style="float:left; margin-right:10px; margin-bottom: 10px;" id="img_{{ $image->id }}">
                        <div class="panel p6 pbn">
                            <div class="of-h">
                                <img src="{{ $image->image->url('thumb') }}" class="img-responsive" title="stairs-to_soul.jpg">
                                <div class="row table-layout">
                                    <div class="col-xs-8 va-m pln">
                                        <h6>{{ $image->image->originalFilename() }}</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>