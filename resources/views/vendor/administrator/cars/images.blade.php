<div class="panel mt50" id="spy2">
    <div class="panel-heading">
        <span class="panel-icon"><i class="fa fa-upload"></i></span>
        <span class="panel-title"> {{ trans('administrator::columns.global.images') }}</span>
    </div>
    <div class="panel-body">
        <input type="file" name="images[]" id="images_gallery" multiple/>
        <div class="body gallery-page">
            <div id="mix-container" class="grid" style="margin-top: 10px;">
                <span class="hide" data-url="" id="url_remove_image"></span>
                @if(isset($images) && $images)
                    @foreach($images as $image)
                        <div class="mix label2 folder2" style="float:left; margin-right:10px; margin-bottom: 10px;" id="img_{{ $image->id }}">
                            <div class="panel p6 pbn">
                                <div class="of-h">
                                    <img src="{{ $image->image->url('thumb') }}" class="img-responsive" title="stairs-to_soul.jpg">
                                    <div class="row table-layout">
                                        <div class="col-xs-8 va-m pln">
                                            <h6>{{ $image->image->originalFilename() }}</h6>
                                        </div>
                                        <div class="col-xs-4 text-right va-m prn">
                                            <a href="{{ route('scaffold.delete_attachment', ['cars', $image->car_id, $image->id]) }}" onclick="return confirm('Delete this image?');" class="fa fa-trash-o fs20 text-alert ml10 delete_image" data-id="{{ $image->id }}"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</div>