@if(isset($topMenuItems))
    @foreach($topMenuItems as $item)
        <a href="{{ $item->page->key ? route($item->page->key) : route('page', $item->salg) }}">{{ $item->name }}</a>
    @endforeach
@endif