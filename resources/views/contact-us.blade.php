@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            {!! Form::open(['class' => 'form-horizontal', 'method' => 'POST', 'route' => 'send-message']) !!}
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                {!! Form::label('email', trans('forms.email'), ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::email('email', old('email'), ['class' => 'form-control']) !!}
                    @if ($errors->has('email'))
                        <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                {!! Form::label('name', trans('forms.name'), ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::text('name', old('name'), ['class' => 'form-control']) !!}
                    @if ($errors->has('name'))
                        <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                {!! Form::label('phone', trans('forms.phone'), ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::text('phone', old('phone'), ['class' => 'form-control']) !!}
                    @if ($errors->has('phone'))
                        <span class="help-block"><strong>{{ $errors->first('phone') }}</strong></span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
                {!! Form::label('message', trans('forms.message'), ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::textarea('message', old('message'), ['class' => 'form-control']) !!}
                    @if ($errors->has('phone'))
                        <span class="help-block"><strong>{{ $errors->first('message') }}</strong></span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-8 col-md-offset-4">
                    {{ Form::submit(trans('buttons.send_message'), ['class' => 'btn btn-primary']) }}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
