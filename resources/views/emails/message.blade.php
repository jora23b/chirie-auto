<table>
    <tr>
        <td><strong>Nume:</strong></td>
        <td>{{ $messageModel->name }}</td>
    </tr>
    <tr>
        <td><strong>Telefon:</strong></td>
        <td>{{ $messageModel->phone }}</td>
    </tr>
    <tr>
        <td><strong>Email:</strong></td>
        <td>{{ $messageModel->email }}</td>
    </tr>
    <tr>
        <td><strong>Nume:</strong></td>
        <td>{{ $messageModel->name }}</td>
    </tr>
    <tr>
        <td><strong>Masina:</strong></td>
        <td>
            @if($messageModel->page_id)
                <a href="{{ $messageModel->car->url() }}">{{ $messageModel->car->presenter->getTitle() }}</a>
            @endif
        </td>
    </tr>
    <tr>
        <td><strong>Mesaj:</strong></td>
        <td>{{ $messageModel->message }}</td>
    </tr>
    <tr>
        <td><strong>Data:</strong></td>
        <td>{{ $messageModel->created_at }}</td>
    </tr>
</table>
