<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->increments('id');
            $table->string('model');
            $table->string('mark');
            $table->string('slug');
            $table->string('type')->nullable();
            $table->decimal('price', 10, 2)->nullable();
            $table->unsignedInteger('year')->nullable();
            $table->unsignedInteger('capacity')->nullable();
            $table->string('fuel')->nullable();
            $table->string('transmission')->nullable();
            $table->unsignedInteger('mileage')->nullable();
            $table->unsignedInteger('rank')->nullable();
            $table->boolean('home_page');
            $table->boolean('active');
            $table->timestamps();
        });

        Schema::create('car_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('car_id');
            $table->foreign('car_id')->references('id')->on('cars')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedInteger('language_id');
            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate('cascade');
            $table->string('slug', 255);
            $table->string('color')->nullable();
            $table->longText('description')->nullable();
            $table->string('meta_title', 255)->nullable();
            $table->string('meta_description', 255)->nullable();
            $table->string('meta_keywords', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_translations');
        Schema::dropIfExists('cars');
    }
}
