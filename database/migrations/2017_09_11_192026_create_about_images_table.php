<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAboutImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('about_images', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('rank')->nullable();
            $table->boolean('active');
            $table->timestamps();
        });

        Schema::create('about_image_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('about_image_id');
            $table->foreign('about_image_id')->references('id')->on('about_images')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedInteger('language_id');
            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate('cascade');
            $table->string('name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('about_image_translations');
        Schema::dropIfExists('about_images');
    }
}
