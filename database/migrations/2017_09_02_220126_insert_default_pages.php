<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertDefaultPages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $items = [1 => 'rent-cars', 2 => 'sale-cars', 3 => 'contact-us', 4 => 'home-page', 5 => 'about-us'];

        foreach ($items as $id => $item) {
            DB::table('pages')->insert([
                'id' => $id,
                'key' => $item,
                'active' => 1,
                'created_at' => date('Y-m-d H:i:s', time()),
            ]);

            foreach (\localizer\locales() as $locale) {
                DB::table('page_translations')->insert([
                    'page_id' => $id,
                    'language_id' => $locale->id(),
                    'title' => $item,
                    'slug' => $item,
                ]);
            }
        }
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('pages')->delete();
    }
}
