<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlidesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slides', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('rank')->nullable();
            $table->boolean('active');
            $table->timestamps();
        });

        Schema::create('slide_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('slide_id');
            $table->foreign('slide_id')->references('id')->on('slides')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedInteger('language_id');
            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate('cascade');
            $table->string('title')->nullable();
            $table->string('description', 255)->nullable();
            $table->string('link', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slide_translations');
        Schema::dropIfExists('slides');
    }
}
