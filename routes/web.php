<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Routing\Router;

Route::group(['prefix' => 'cms'], function (Router $router) {
    Route::group(['prefix' => 'translations'], function (Router $router) {
        $router->get('/')->name('translations.index')->uses('Admin\TranslationsController@index');
        $router->post('/')->name('translations.store')->uses('Admin\TranslationsController@store');
    });
});

\localizer\group(['middleware' => 'guest'], function () {
    Route::get('/', ['as' => 'home-page', 'uses' => 'HomePageController@index']);
    Route::get(trans('routes.rent-cars'), ['as' => 'rent-cars', 'uses' => 'CarsController@index']);
    Route::get(trans('routes.sale-cars'), ['as' => 'sale-cars', 'uses' => 'CarsController@index']);
    Route::get(trans('routes.about-us'), ['as' => 'about-us', 'uses' => 'AboutUsController@index']);
    Route::get(trans('routes.contact-us'), ['as' => 'contact-us', 'uses' => 'ContactUsController@index']);

    Route::get(trans('routes.rent-cars') . '/{car}', ['as' => 'rent-car', 'uses' => 'CarsController@index']);
    Route::get(trans('routes.sale-cars') . '/{car}', ['as' => 'sale-car', 'uses' => 'CarsController@index']);

    Route::post('send-message', ['as' => 'send-message', 'uses' => 'MessagesController@store']);
    Route::get('page/{page}', ['as' => 'page', 'uses' => 'PageController@index']);
});

Route::get('sitemap.xml', ['as' => 'sitemap', 'uses' => 'SiteMapController@index']);
