/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 66);
/******/ })
/************************************************************************/
/******/ ({

/***/ 66:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(67);


/***/ }),

/***/ 67:
/***/ (function(module, exports) {

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var AdminArchitect = function () {
    function AdminArchitect() {
        _classCallCheck(this, AdminArchitect);

        ['SidebarNavigation', 'Panels', 'Collections', 'BatchActions', 'DateControls', 'LiveSearch', 'Fancybox'].map(function (method) {
            AdminArchitect['handle' + method].call();
        });
    }

    _createClass(AdminArchitect, null, [{
        key: 'handleSidebarNavigation',
        value: function handleSidebarNavigation() {
            var toggleMenu = function toggleMenu(marginLeft, marginMain) {
                var emailList = $(window).width() <= 768 && $(window).width() > 640 ? 320 : 360;

                if ($('.mainpanel').css('position') === 'relative') {
                    $('.logopanel, .leftpanel').animate({ left: marginLeft }, 'fast');
                    $('.headerbar, .mainpanel').animate({ left: marginMain }, 'fast');

                    $('.emailcontent, .email-options').animate({ left: marginMain }, 'fast');
                    $('.emailpanel').animate({ left: marginMain + emailList }, 'fast');

                    var $body = $('body');
                    if ('hidden' === $body.css('overflow')) {
                        $body.css({ overflow: '' });
                    } else {
                        $body.css({ overflow: 'hidden' });
                    }
                } else {
                    $('.logopanel, .leftpanel').animate({ marginLeft: marginLeft }, 'fast');
                    $('.headerbar, .mainpanel').animate({ marginLeft: marginMain }, 'fast');

                    $('.emailcontent, .email-options').animate({ left: marginMain }, 'fast');
                    $('.emailpanel').animate({ left: marginMain + emailList }, 'fast');
                }
            };

            $('#menuToggle').click(function () {
                var $panel = $('.mainpanel');
                var collapsedMargin = $panel.css('margin-left');
                var collapsedLeft = $panel.css('left');

                if (collapsedMargin === '220px' || collapsedLeft === '220px') {
                    toggleMenu(-220, 0);
                } else {
                    toggleMenu(0, 220);
                }
            });

            $('.nav-parent > a').on('click', function (_ref) {
                var target = _ref.target;

                var $target = $(target);

                var gran = $target.closest('.nav');
                var parent = $target.parent();
                var sub = parent.find('> ul');

                if (sub.is(':visible')) {
                    sub.slideUp(200);
                    if (parent.hasClass('nav-active')) {
                        parent.removeClass('nav-active');
                    }
                } else {
                    $(gran).find('.children').each(function (i, e) {
                        $(e).slideUp();
                    });

                    sub.slideDown(200);

                    if (!parent.hasClass('active')) {
                        parent.addClass('nav-active');
                    }
                }
                return false;
            });
        }
    }, {
        key: 'handlePanels',
        value: function handlePanels() {
            // Close panel
            $('.panel-remove').click(function (_ref2) {
                var target = _ref2.target;

                $(target).closest('.panel').fadeOut(function (_ref3) {
                    var target = _ref3.target;

                    $(target).remove();
                });
            });

            // Minimize panel
            $('.panel-minimize').click(function (_ref4) {
                var target = _ref4.target;

                var parent = $(target).closest('.panel');

                parent.find('.panel-body').slideToggle(function () {
                    var panelHeading = parent.find('.panel-heading');

                    if (panelHeading.hasClass('min')) {
                        panelHeading.removeClass('min');
                    } else {
                        panelHeading.addClass('min');
                    }
                });
            });
        }
    }, {
        key: 'handleCollections',
        value: function handleCollections() {
            $(document).on('click', '.toggle-collection', function (_ref5) {
                var target = _ref5.target;

                var fn = $(target);

                $('input[type=checkbox].collection-item').each(function (i, e) {
                    $(e).prop('checked', fn.prop('checked'));
                });
            });
        }
    }, {
        key: 'handleBatchActions',
        value: function handleBatchActions() {
            var selected = function selected() {
                return $('input[type=checkbox]:checked.collection-item');
            };

            $(document).on('click', '.batch-actions a[data-action]', function (_ref6) {
                var target = _ref6.target;

                if (!selected().length) {
                    return false;
                }

                var $target = $(target);

                if ((msg = $target.data('confirmation')) && !window.confirm(msg)) {
                    return false;
                }

                $('#batch_action').val($target.data('action'));
                $('#collection').submit();

                return false;
            });
        }
    }, {
        key: 'handleDateControls',
        value: function handleDateControls() {
            $('[data-filter-type="date"]').datepicker({
                format: 'yyyy-mm-dd',
                clearBtn: false,
                multidate: false
            });

            $('[data-filter-type="daterange"]').daterangepicker({
                locale: {
                    format: 'YYYY-MM-DD'
                },
                autoUpdateInput: false,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }).on('apply.daterangepicker', function (ev, picker) {
                $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
            }).on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
            });
        }
    }, {
        key: 'handleLiveSearch',
        value: function handleLiveSearch() {
            $('[data-type="livesearch"]').selectize({
                valueField: 'id',
                labelField: 'name',
                searchField: ['name'],
                create: false,
                loadThrottle: 500,
                maxOptions: 100,
                load: function load(query, callback) {
                    if (!query.length >= 3) return callback();

                    var selectize = $($(this)[0].$input);

                    var baseUrl = selectize.data('url');
                    var url = baseUrl + (-1 === baseUrl.indexOf('?') ? '?' : '&');
                    url += 'query=' + query;

                    $.ajax({
                        url: url,
                        type: 'GET',
                        error: callback,
                        success: function success(res) {
                            if (!res.hasOwnProperty('items')) {
                                console.error('Livesearch response should have "items" collection. ' + 'Each element in collection must have at least 2 keys: "id" and "name"');

                                return false;
                            }

                            return callback(res.items);
                        }
                    });
                }
            });
        }
    }, {
        key: 'handleFancybox',
        value: function handleFancybox() {
            $('.fancybox').fancybox({
                afterLoad: function afterLoad() {
                    var width = void 0,
                        height = void 0;
                    if (width = $(this.element).data('width')) {
                        this.width = width;
                    }

                    if (height = $(this.element).data('height')) {
                        this.height = height;
                    }
                }
            });
        }
    }]);

    return AdminArchitect;
}();

$(function () {
    return new AdminArchitect();
});

function test() {
    alert('am');
}

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgNzJkNzcxYjQ3MmFlNWI3MDlhZjMiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9hZG1pbmlzdHJhdG9yL2pzL2FwcC5qcyJdLCJuYW1lcyI6WyJBZG1pbkFyY2hpdGVjdCIsIm1hcCIsIm1ldGhvZCIsImNhbGwiLCJ0b2dnbGVNZW51IiwibWFyZ2luTGVmdCIsIm1hcmdpbk1haW4iLCJlbWFpbExpc3QiLCIkIiwid2luZG93Iiwid2lkdGgiLCJjc3MiLCJhbmltYXRlIiwibGVmdCIsIiRib2R5Iiwib3ZlcmZsb3ciLCJjbGljayIsIiRwYW5lbCIsImNvbGxhcHNlZE1hcmdpbiIsImNvbGxhcHNlZExlZnQiLCJvbiIsInRhcmdldCIsIiR0YXJnZXQiLCJncmFuIiwiY2xvc2VzdCIsInBhcmVudCIsInN1YiIsImZpbmQiLCJpcyIsInNsaWRlVXAiLCJoYXNDbGFzcyIsInJlbW92ZUNsYXNzIiwiZWFjaCIsImkiLCJlIiwic2xpZGVEb3duIiwiYWRkQ2xhc3MiLCJmYWRlT3V0IiwicmVtb3ZlIiwic2xpZGVUb2dnbGUiLCJwYW5lbEhlYWRpbmciLCJkb2N1bWVudCIsImZuIiwicHJvcCIsInNlbGVjdGVkIiwibGVuZ3RoIiwibXNnIiwiZGF0YSIsImNvbmZpcm0iLCJ2YWwiLCJzdWJtaXQiLCJkYXRlcGlja2VyIiwiZm9ybWF0IiwiY2xlYXJCdG4iLCJtdWx0aWRhdGUiLCJkYXRlcmFuZ2VwaWNrZXIiLCJsb2NhbGUiLCJhdXRvVXBkYXRlSW5wdXQiLCJyYW5nZXMiLCJtb21lbnQiLCJzdWJ0cmFjdCIsInN0YXJ0T2YiLCJlbmRPZiIsImV2IiwicGlja2VyIiwic3RhcnREYXRlIiwiZW5kRGF0ZSIsInNlbGVjdGl6ZSIsInZhbHVlRmllbGQiLCJsYWJlbEZpZWxkIiwic2VhcmNoRmllbGQiLCJjcmVhdGUiLCJsb2FkVGhyb3R0bGUiLCJtYXhPcHRpb25zIiwibG9hZCIsInF1ZXJ5IiwiY2FsbGJhY2siLCIkaW5wdXQiLCJiYXNlVXJsIiwidXJsIiwiaW5kZXhPZiIsImFqYXgiLCJ0eXBlIiwiZXJyb3IiLCJzdWNjZXNzIiwicmVzIiwiaGFzT3duUHJvcGVydHkiLCJjb25zb2xlIiwiaXRlbXMiLCJmYW5jeWJveCIsImFmdGVyTG9hZCIsImhlaWdodCIsImVsZW1lbnQiLCJ0ZXN0IiwiYWxlcnQiXSwibWFwcGluZ3MiOiI7QUFBQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7O0FBRUE7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUM3RE1BLGM7QUFDRiw4QkFBYztBQUFBOztBQUNWLFNBQ0ksbUJBREosRUFDeUIsUUFEekIsRUFDbUMsYUFEbkMsRUFDa0QsY0FEbEQsRUFFSSxjQUZKLEVBRW9CLFlBRnBCLEVBRWtDLFVBRmxDLEVBR0VDLEdBSEYsQ0FHTSxVQUFDQyxNQUFELEVBQVk7QUFDZEYsMkJBQWUsV0FBV0UsTUFBMUIsRUFBa0NDLElBQWxDO0FBQ0gsU0FMRDtBQU1IOzs7O2tEQUVnQztBQUM3QixnQkFBTUMsYUFBYSxTQUFiQSxVQUFhLENBQUNDLFVBQUQsRUFBYUMsVUFBYixFQUE0QjtBQUMzQyxvQkFBSUMsWUFBYUMsRUFBRUMsTUFBRixFQUFVQyxLQUFWLE1BQXFCLEdBQXJCLElBQTRCRixFQUFFQyxNQUFGLEVBQVVDLEtBQVYsS0FDekMsR0FEWSxHQUNMLEdBREssR0FDQyxHQURqQjs7QUFHQSxvQkFBSUYsRUFBRSxZQUFGLEVBQWdCRyxHQUFoQixDQUFvQixVQUFwQixNQUFvQyxVQUF4QyxFQUFvRDtBQUNoREgsc0JBQUUsd0JBQUYsRUFBNEJJLE9BQTVCLENBQW9DLEVBQUNDLE1BQU1SLFVBQVAsRUFBcEMsRUFBd0QsTUFBeEQ7QUFDQUcsc0JBQUUsd0JBQUYsRUFBNEJJLE9BQTVCLENBQW9DLEVBQUNDLE1BQU1QLFVBQVAsRUFBcEMsRUFBd0QsTUFBeEQ7O0FBRUFFLHNCQUFFLCtCQUFGLEVBQW1DSSxPQUFuQyxDQUEyQyxFQUFDQyxNQUFNUCxVQUFQLEVBQTNDLEVBQStELE1BQS9EO0FBQ0FFLHNCQUFFLGFBQUYsRUFBaUJJLE9BQWpCLENBQXlCLEVBQUNDLE1BQU1QLGFBQWFDLFNBQXBCLEVBQXpCLEVBQXlELE1BQXpEOztBQUVBLHdCQUFJTyxRQUFRTixFQUFFLE1BQUYsQ0FBWjtBQUNBLHdCQUFJLGFBQWFNLE1BQU1ILEdBQU4sQ0FBVSxVQUFWLENBQWpCLEVBQXdDO0FBQ3BDRyw4QkFBTUgsR0FBTixDQUFVLEVBQUNJLFVBQVUsRUFBWCxFQUFWO0FBQ0gscUJBRkQsTUFFTztBQUNIRCw4QkFBTUgsR0FBTixDQUFVLEVBQUNJLFVBQVUsUUFBWCxFQUFWO0FBQ0g7QUFDSixpQkFiRCxNQWFPO0FBQ0hQLHNCQUFFLHdCQUFGLEVBQTRCSSxPQUE1QixDQUFvQyxFQUFDUCxZQUFZQSxVQUFiLEVBQXBDLEVBQThELE1BQTlEO0FBQ0FHLHNCQUFFLHdCQUFGLEVBQTRCSSxPQUE1QixDQUFvQyxFQUFDUCxZQUFZQyxVQUFiLEVBQXBDLEVBQThELE1BQTlEOztBQUVBRSxzQkFBRSwrQkFBRixFQUFtQ0ksT0FBbkMsQ0FBMkMsRUFBQ0MsTUFBTVAsVUFBUCxFQUEzQyxFQUErRCxNQUEvRDtBQUNBRSxzQkFBRSxhQUFGLEVBQWlCSSxPQUFqQixDQUF5QixFQUFDQyxNQUFNUCxhQUFhQyxTQUFwQixFQUF6QixFQUF5RCxNQUF6RDtBQUNIO0FBQ0osYUF4QkQ7O0FBMEJBQyxjQUFFLGFBQUYsRUFBaUJRLEtBQWpCLENBQXVCLFlBQU07QUFDekIsb0JBQUlDLFNBQVNULEVBQUUsWUFBRixDQUFiO0FBQ0Esb0JBQUlVLGtCQUFrQkQsT0FBT04sR0FBUCxDQUFXLGFBQVgsQ0FBdEI7QUFDQSxvQkFBSVEsZ0JBQWdCRixPQUFPTixHQUFQLENBQVcsTUFBWCxDQUFwQjs7QUFFQSxvQkFBSU8sb0JBQW9CLE9BQXBCLElBQStCQyxrQkFBa0IsT0FBckQsRUFBOEQ7QUFDMURmLCtCQUFXLENBQUMsR0FBWixFQUFpQixDQUFqQjtBQUNILGlCQUZELE1BRU87QUFDSEEsK0JBQVcsQ0FBWCxFQUFjLEdBQWQ7QUFDSDtBQUNKLGFBVkQ7O0FBWUFJLGNBQUUsaUJBQUYsRUFBcUJZLEVBQXJCLENBQXdCLE9BQXhCLEVBQWlDLGdCQUFjO0FBQUEsb0JBQVpDLE1BQVksUUFBWkEsTUFBWTs7QUFDM0Msb0JBQU1DLFVBQVVkLEVBQUVhLE1BQUYsQ0FBaEI7O0FBRUEsb0JBQUlFLE9BQU9ELFFBQVFFLE9BQVIsQ0FBZ0IsTUFBaEIsQ0FBWDtBQUNBLG9CQUFJQyxTQUFTSCxRQUFRRyxNQUFSLEVBQWI7QUFDQSxvQkFBSUMsTUFBTUQsT0FBT0UsSUFBUCxDQUFZLE1BQVosQ0FBVjs7QUFFQSxvQkFBSUQsSUFBSUUsRUFBSixDQUFPLFVBQVAsQ0FBSixFQUF3QjtBQUNwQkYsd0JBQUlHLE9BQUosQ0FBWSxHQUFaO0FBQ0Esd0JBQUlKLE9BQU9LLFFBQVAsQ0FBZ0IsWUFBaEIsQ0FBSixFQUFtQztBQUMvQkwsK0JBQU9NLFdBQVAsQ0FBbUIsWUFBbkI7QUFDSDtBQUNKLGlCQUxELE1BS087QUFDSHZCLHNCQUFFZSxJQUFGLEVBQVFJLElBQVIsQ0FBYSxXQUFiLEVBQTBCSyxJQUExQixDQUErQixVQUFDQyxDQUFELEVBQUlDLENBQUosRUFBVTtBQUNyQzFCLDBCQUFFMEIsQ0FBRixFQUFLTCxPQUFMO0FBQ0gscUJBRkQ7O0FBSUFILHdCQUFJUyxTQUFKLENBQWMsR0FBZDs7QUFFQSx3QkFBSSxDQUFDVixPQUFPSyxRQUFQLENBQWdCLFFBQWhCLENBQUwsRUFBZ0M7QUFDNUJMLCtCQUFPVyxRQUFQLENBQWdCLFlBQWhCO0FBQ0g7QUFDSjtBQUNELHVCQUFPLEtBQVA7QUFDSCxhQXhCRDtBQXlCSDs7O3VDQUVxQjtBQUNsQjtBQUNBNUIsY0FBRSxlQUFGLEVBQW1CUSxLQUFuQixDQUF5QixpQkFBYztBQUFBLG9CQUFaSyxNQUFZLFNBQVpBLE1BQVk7O0FBQ25DYixrQkFBRWEsTUFBRixFQUFVRyxPQUFWLENBQWtCLFFBQWxCLEVBQTRCYSxPQUE1QixDQUFvQyxpQkFBYztBQUFBLHdCQUFaaEIsTUFBWSxTQUFaQSxNQUFZOztBQUM5Q2Isc0JBQUVhLE1BQUYsRUFBVWlCLE1BQVY7QUFDSCxpQkFGRDtBQUdILGFBSkQ7O0FBTUE7QUFDQTlCLGNBQUUsaUJBQUYsRUFBcUJRLEtBQXJCLENBQTJCLGlCQUFjO0FBQUEsb0JBQVpLLE1BQVksU0FBWkEsTUFBWTs7QUFDckMsb0JBQU1JLFNBQVNqQixFQUFFYSxNQUFGLEVBQVVHLE9BQVYsQ0FBa0IsUUFBbEIsQ0FBZjs7QUFFQUMsdUJBQU9FLElBQVAsQ0FBWSxhQUFaLEVBQTJCWSxXQUEzQixDQUF1QyxZQUFNO0FBQ3pDLHdCQUFJQyxlQUFlZixPQUFPRSxJQUFQLENBQVksZ0JBQVosQ0FBbkI7O0FBRUEsd0JBQUlhLGFBQWFWLFFBQWIsQ0FBc0IsS0FBdEIsQ0FBSixFQUFrQztBQUM5QlUscUNBQWFULFdBQWIsQ0FBeUIsS0FBekI7QUFDSCxxQkFGRCxNQUVPO0FBQ0hTLHFDQUFhSixRQUFiLENBQXNCLEtBQXRCO0FBQ0g7QUFDSixpQkFSRDtBQVNILGFBWkQ7QUFhSDs7OzRDQUUwQjtBQUN2QjVCLGNBQUVpQyxRQUFGLEVBQVlyQixFQUFaLENBQWUsT0FBZixFQUF3QixvQkFBeEIsRUFBOEMsaUJBQWM7QUFBQSxvQkFBWkMsTUFBWSxTQUFaQSxNQUFZOztBQUN4RCxvQkFBTXFCLEtBQUtsQyxFQUFFYSxNQUFGLENBQVg7O0FBRUFiLGtCQUFFLHNDQUFGLEVBQTBDd0IsSUFBMUMsQ0FBK0MsVUFBQ0MsQ0FBRCxFQUFJQyxDQUFKLEVBQVU7QUFDckQxQixzQkFBRTBCLENBQUYsRUFBS1MsSUFBTCxDQUFVLFNBQVYsRUFBcUJELEdBQUdDLElBQUgsQ0FBUSxTQUFSLENBQXJCO0FBQ0gsaUJBRkQ7QUFHSCxhQU5EO0FBT0g7Ozs2Q0FFMkI7QUFDeEIsZ0JBQU1DLFdBQVcsU0FBWEEsUUFBVyxHQUFNO0FBQ25CLHVCQUFPcEMsRUFBRSw4Q0FBRixDQUFQO0FBQ0gsYUFGRDs7QUFJQUEsY0FBRWlDLFFBQUYsRUFBWXJCLEVBQVosQ0FBZSxPQUFmLEVBQXdCLCtCQUF4QixFQUF5RCxpQkFBYztBQUFBLG9CQUFaQyxNQUFZLFNBQVpBLE1BQVk7O0FBQ25FLG9CQUFJLENBQUN1QixXQUFXQyxNQUFoQixFQUF3QjtBQUNwQiwyQkFBTyxLQUFQO0FBQ0g7O0FBRUQsb0JBQU12QixVQUFVZCxFQUFFYSxNQUFGLENBQWhCOztBQUVBLG9CQUFJLENBQUN5QixNQUFNeEIsUUFBUXlCLElBQVIsQ0FBYSxjQUFiLENBQVAsS0FBd0MsQ0FBQ3RDLE9BQU91QyxPQUFQLENBQWVGLEdBQWYsQ0FBN0MsRUFBa0U7QUFDOUQsMkJBQU8sS0FBUDtBQUNIOztBQUVEdEMsa0JBQUUsZUFBRixFQUFtQnlDLEdBQW5CLENBQXVCM0IsUUFBUXlCLElBQVIsQ0FBYSxRQUFiLENBQXZCO0FBQ0F2QyxrQkFBRSxhQUFGLEVBQWlCMEMsTUFBakI7O0FBRUEsdUJBQU8sS0FBUDtBQUNILGFBZkQ7QUFnQkg7Ozs2Q0FFMkI7QUFDeEIxQyxjQUFFLDJCQUFGLEVBQStCMkMsVUFBL0IsQ0FBMEM7QUFDdENDLHdCQUFRLFlBRDhCO0FBRXRDQywwQkFBVSxLQUY0QjtBQUd0Q0MsMkJBQVc7QUFIMkIsYUFBMUM7O0FBTUE5QyxjQUFFLGdDQUFGLEVBQW9DK0MsZUFBcEMsQ0FBb0Q7QUFDaERDLHdCQUFRO0FBQ0pKLDRCQUFRO0FBREosaUJBRHdDO0FBSWhESyxpQ0FBaUIsS0FKK0I7QUFLaERDLHdCQUFRO0FBQ0osNkJBQVMsQ0FBQ0MsUUFBRCxFQUFXQSxRQUFYLENBREw7QUFFSixpQ0FBYSxDQUNUQSxTQUFTQyxRQUFULENBQWtCLENBQWxCLEVBQXFCLE1BQXJCLENBRFMsRUFFVEQsU0FBU0MsUUFBVCxDQUFrQixDQUFsQixFQUFxQixNQUFyQixDQUZTLENBRlQ7QUFNSixtQ0FBZSxDQUFDRCxTQUFTQyxRQUFULENBQWtCLENBQWxCLEVBQXFCLE1BQXJCLENBQUQsRUFBK0JELFFBQS9CLENBTlg7QUFPSixvQ0FBZ0IsQ0FBQ0EsU0FBU0MsUUFBVCxDQUFrQixFQUFsQixFQUFzQixNQUF0QixDQUFELEVBQWdDRCxRQUFoQyxDQVBaO0FBUUosa0NBQWMsQ0FDVkEsU0FBU0UsT0FBVCxDQUFpQixPQUFqQixDQURVLEVBRVZGLFNBQVNHLEtBQVQsQ0FBZSxPQUFmLENBRlUsQ0FSVjtBQVlKLGtDQUFjLENBQ1ZILFNBQVNDLFFBQVQsQ0FBa0IsQ0FBbEIsRUFBcUIsT0FBckIsRUFBOEJDLE9BQTlCLENBQXNDLE9BQXRDLENBRFUsRUFFVkYsU0FBU0MsUUFBVCxDQUFrQixDQUFsQixFQUFxQixPQUFyQixFQUE4QkUsS0FBOUIsQ0FBb0MsT0FBcEMsQ0FGVTtBQVpWO0FBTHdDLGFBQXBELEVBc0JHMUMsRUF0QkgsQ0FzQk0sdUJBdEJOLEVBc0IrQixVQUFTMkMsRUFBVCxFQUFhQyxNQUFiLEVBQXFCO0FBQ2hEeEQsa0JBQUUsSUFBRixFQUFReUMsR0FBUixDQUFZZSxPQUFPQyxTQUFQLENBQWlCYixNQUFqQixDQUF3QixZQUF4QixJQUF3QyxLQUF4QyxHQUFnRFksT0FBT0UsT0FBUCxDQUFlZCxNQUFmLENBQXNCLFlBQXRCLENBQTVEO0FBQ0gsYUF4QkQsRUF3QkdoQyxFQXhCSCxDQXdCTSx3QkF4Qk4sRUF3QmdDLFVBQVMyQyxFQUFULEVBQWFDLE1BQWIsRUFBcUI7QUFDakR4RCxrQkFBRSxJQUFGLEVBQVF5QyxHQUFSLENBQVksRUFBWjtBQUNILGFBMUJEO0FBMkJIOzs7MkNBRXlCO0FBQ3RCekMsY0FBRSwwQkFBRixFQUE4QjJELFNBQTlCLENBQXdDO0FBQ3BDQyw0QkFBWSxJQUR3QjtBQUVwQ0MsNEJBQVksTUFGd0I7QUFHcENDLDZCQUFhLENBQUMsTUFBRCxDQUh1QjtBQUlwQ0Msd0JBQVEsS0FKNEI7QUFLcENDLDhCQUFjLEdBTHNCO0FBTXBDQyw0QkFBWSxHQU53QjtBQU9wQ0Msc0JBQU0sY0FBU0MsS0FBVCxFQUFnQkMsUUFBaEIsRUFBMEI7QUFDNUIsd0JBQUksQ0FBQ0QsTUFBTTlCLE1BQVAsSUFBaUIsQ0FBckIsRUFBd0IsT0FBTytCLFVBQVA7O0FBRXhCLHdCQUFJVCxZQUFZM0QsRUFBRUEsRUFBRSxJQUFGLEVBQVEsQ0FBUixFQUFXcUUsTUFBYixDQUFoQjs7QUFFQSx3QkFBSUMsVUFBVVgsVUFBVXBCLElBQVYsQ0FBZSxLQUFmLENBQWQ7QUFDQSx3QkFBSWdDLE1BQU1ELFdBQVcsQ0FBQyxDQUFELEtBQU9BLFFBQVFFLE9BQVIsQ0FBZ0IsR0FBaEIsQ0FBUCxHQUE4QixHQUE5QixHQUFvQyxHQUEvQyxDQUFWO0FBQ0FELDJCQUFPLFdBQVdKLEtBQWxCOztBQUVBbkUsc0JBQUV5RSxJQUFGLENBQU87QUFDSEYsNkJBQUtBLEdBREY7QUFFSEcsOEJBQU0sS0FGSDtBQUdIQywrQkFBT1AsUUFISjtBQUlIUSxpQ0FBUyxpQkFBU0MsR0FBVCxFQUFjO0FBQ25CLGdDQUFJLENBQUNBLElBQUlDLGNBQUosQ0FBbUIsT0FBbkIsQ0FBTCxFQUFrQztBQUM5QkMsd0NBQVFKLEtBQVIsQ0FDSSx5REFDQSx1RUFGSjs7QUFLQSx1Q0FBTyxLQUFQO0FBQ0g7O0FBRUQsbUNBQU9QLFNBQVNTLElBQUlHLEtBQWIsQ0FBUDtBQUNIO0FBZkUscUJBQVA7QUFpQkg7QUFqQ21DLGFBQXhDO0FBbUNIOzs7eUNBRXVCO0FBQ3BCaEYsY0FBRSxXQUFGLEVBQWVpRixRQUFmLENBQXdCO0FBQ3BCQywyQkFBVyxxQkFBVztBQUNsQix3QkFBSWhGLGNBQUo7QUFBQSx3QkFBV2lGLGVBQVg7QUFDQSx3QkFBSWpGLFFBQVFGLEVBQUUsS0FBS29GLE9BQVAsRUFBZ0I3QyxJQUFoQixDQUFxQixPQUFyQixDQUFaLEVBQTJDO0FBQ3ZDLDZCQUFLckMsS0FBTCxHQUFhQSxLQUFiO0FBQ0g7O0FBRUQsd0JBQUlpRixTQUFTbkYsRUFBRSxLQUFLb0YsT0FBUCxFQUFnQjdDLElBQWhCLENBQXFCLFFBQXJCLENBQWIsRUFBNkM7QUFDekMsNkJBQUs0QyxNQUFMLEdBQWNBLE1BQWQ7QUFDSDtBQUNKO0FBVm1CLGFBQXhCO0FBWUg7Ozs7OztBQUdMbkYsRUFBRTtBQUFBLFdBQU0sSUFBSVIsY0FBSixFQUFOO0FBQUEsQ0FBRjs7QUFFQSxTQUFTNkYsSUFBVCxHQUFnQjtBQUNaQyxVQUFNLElBQU47QUFDSCxDIiwiZmlsZSI6IlxcYWRtaW5cXGFwcC5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDY2KTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyB3ZWJwYWNrL2Jvb3RzdHJhcCA3MmQ3NzFiNDcyYWU1YjcwOWFmMyIsImNsYXNzIEFkbWluQXJjaGl0ZWN0IHtcbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgW1xuICAgICAgICAgICAgJ1NpZGViYXJOYXZpZ2F0aW9uJywgJ1BhbmVscycsICdDb2xsZWN0aW9ucycsICdCYXRjaEFjdGlvbnMnLFxuICAgICAgICAgICAgJ0RhdGVDb250cm9scycsICdMaXZlU2VhcmNoJywgJ0ZhbmN5Ym94JyxcbiAgICAgICAgXS5tYXAoKG1ldGhvZCkgPT4ge1xuICAgICAgICAgICAgQWRtaW5BcmNoaXRlY3RbJ2hhbmRsZScgKyBtZXRob2RdLmNhbGwoKTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgc3RhdGljIGhhbmRsZVNpZGViYXJOYXZpZ2F0aW9uKCkge1xuICAgICAgICBjb25zdCB0b2dnbGVNZW51ID0gKG1hcmdpbkxlZnQsIG1hcmdpbk1haW4pID0+IHtcbiAgICAgICAgICAgIGxldCBlbWFpbExpc3QgPSAoJCh3aW5kb3cpLndpZHRoKCkgPD0gNzY4ICYmICQod2luZG93KS53aWR0aCgpID5cbiAgICAgICAgICAgICAgICA2NDApID8gMzIwIDogMzYwO1xuXG4gICAgICAgICAgICBpZiAoJCgnLm1haW5wYW5lbCcpLmNzcygncG9zaXRpb24nKSA9PT0gJ3JlbGF0aXZlJykge1xuICAgICAgICAgICAgICAgICQoJy5sb2dvcGFuZWwsIC5sZWZ0cGFuZWwnKS5hbmltYXRlKHtsZWZ0OiBtYXJnaW5MZWZ0fSwgJ2Zhc3QnKTtcbiAgICAgICAgICAgICAgICAkKCcuaGVhZGVyYmFyLCAubWFpbnBhbmVsJykuYW5pbWF0ZSh7bGVmdDogbWFyZ2luTWFpbn0sICdmYXN0Jyk7XG5cbiAgICAgICAgICAgICAgICAkKCcuZW1haWxjb250ZW50LCAuZW1haWwtb3B0aW9ucycpLmFuaW1hdGUoe2xlZnQ6IG1hcmdpbk1haW59LCAnZmFzdCcpO1xuICAgICAgICAgICAgICAgICQoJy5lbWFpbHBhbmVsJykuYW5pbWF0ZSh7bGVmdDogbWFyZ2luTWFpbiArIGVtYWlsTGlzdH0sICdmYXN0Jyk7XG5cbiAgICAgICAgICAgICAgICBsZXQgJGJvZHkgPSAkKCdib2R5Jyk7XG4gICAgICAgICAgICAgICAgaWYgKCdoaWRkZW4nID09PSAkYm9keS5jc3MoJ292ZXJmbG93JykpIHtcbiAgICAgICAgICAgICAgICAgICAgJGJvZHkuY3NzKHtvdmVyZmxvdzogJyd9KTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAkYm9keS5jc3Moe292ZXJmbG93OiAnaGlkZGVuJ30pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgJCgnLmxvZ29wYW5lbCwgLmxlZnRwYW5lbCcpLmFuaW1hdGUoe21hcmdpbkxlZnQ6IG1hcmdpbkxlZnR9LCAnZmFzdCcpO1xuICAgICAgICAgICAgICAgICQoJy5oZWFkZXJiYXIsIC5tYWlucGFuZWwnKS5hbmltYXRlKHttYXJnaW5MZWZ0OiBtYXJnaW5NYWlufSwgJ2Zhc3QnKTtcblxuICAgICAgICAgICAgICAgICQoJy5lbWFpbGNvbnRlbnQsIC5lbWFpbC1vcHRpb25zJykuYW5pbWF0ZSh7bGVmdDogbWFyZ2luTWFpbn0sICdmYXN0Jyk7XG4gICAgICAgICAgICAgICAgJCgnLmVtYWlscGFuZWwnKS5hbmltYXRlKHtsZWZ0OiBtYXJnaW5NYWluICsgZW1haWxMaXN0fSwgJ2Zhc3QnKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcblxuICAgICAgICAkKCcjbWVudVRvZ2dsZScpLmNsaWNrKCgpID0+IHtcbiAgICAgICAgICAgIGxldCAkcGFuZWwgPSAkKCcubWFpbnBhbmVsJyk7XG4gICAgICAgICAgICBsZXQgY29sbGFwc2VkTWFyZ2luID0gJHBhbmVsLmNzcygnbWFyZ2luLWxlZnQnKTtcbiAgICAgICAgICAgIGxldCBjb2xsYXBzZWRMZWZ0ID0gJHBhbmVsLmNzcygnbGVmdCcpO1xuXG4gICAgICAgICAgICBpZiAoY29sbGFwc2VkTWFyZ2luID09PSAnMjIwcHgnIHx8IGNvbGxhcHNlZExlZnQgPT09ICcyMjBweCcpIHtcbiAgICAgICAgICAgICAgICB0b2dnbGVNZW51KC0yMjAsIDApO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICB0b2dnbGVNZW51KDAsIDIyMCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuXG4gICAgICAgICQoJy5uYXYtcGFyZW50ID4gYScpLm9uKCdjbGljaycsICh7dGFyZ2V0fSkgPT4ge1xuICAgICAgICAgICAgY29uc3QgJHRhcmdldCA9ICQodGFyZ2V0KTtcblxuICAgICAgICAgICAgbGV0IGdyYW4gPSAkdGFyZ2V0LmNsb3Nlc3QoJy5uYXYnKTtcbiAgICAgICAgICAgIGxldCBwYXJlbnQgPSAkdGFyZ2V0LnBhcmVudCgpO1xuICAgICAgICAgICAgbGV0IHN1YiA9IHBhcmVudC5maW5kKCc+IHVsJyk7XG5cbiAgICAgICAgICAgIGlmIChzdWIuaXMoJzp2aXNpYmxlJykpIHtcbiAgICAgICAgICAgICAgICBzdWIuc2xpZGVVcCgyMDApO1xuICAgICAgICAgICAgICAgIGlmIChwYXJlbnQuaGFzQ2xhc3MoJ25hdi1hY3RpdmUnKSkge1xuICAgICAgICAgICAgICAgICAgICBwYXJlbnQucmVtb3ZlQ2xhc3MoJ25hdi1hY3RpdmUnKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICQoZ3JhbikuZmluZCgnLmNoaWxkcmVuJykuZWFjaCgoaSwgZSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAkKGUpLnNsaWRlVXAoKTtcbiAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgIHN1Yi5zbGlkZURvd24oMjAwKTtcblxuICAgICAgICAgICAgICAgIGlmICghcGFyZW50Lmhhc0NsYXNzKCdhY3RpdmUnKSkge1xuICAgICAgICAgICAgICAgICAgICBwYXJlbnQuYWRkQ2xhc3MoJ25hdi1hY3RpdmUnKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIHN0YXRpYyBoYW5kbGVQYW5lbHMoKSB7XG4gICAgICAgIC8vIENsb3NlIHBhbmVsXG4gICAgICAgICQoJy5wYW5lbC1yZW1vdmUnKS5jbGljaygoe3RhcmdldH0pID0+IHtcbiAgICAgICAgICAgICQodGFyZ2V0KS5jbG9zZXN0KCcucGFuZWwnKS5mYWRlT3V0KCh7dGFyZ2V0fSkgPT4ge1xuICAgICAgICAgICAgICAgICQodGFyZ2V0KS5yZW1vdmUoKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcblxuICAgICAgICAvLyBNaW5pbWl6ZSBwYW5lbFxuICAgICAgICAkKCcucGFuZWwtbWluaW1pemUnKS5jbGljaygoe3RhcmdldH0pID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHBhcmVudCA9ICQodGFyZ2V0KS5jbG9zZXN0KCcucGFuZWwnKTtcblxuICAgICAgICAgICAgcGFyZW50LmZpbmQoJy5wYW5lbC1ib2R5Jykuc2xpZGVUb2dnbGUoKCkgPT4ge1xuICAgICAgICAgICAgICAgIGxldCBwYW5lbEhlYWRpbmcgPSBwYXJlbnQuZmluZCgnLnBhbmVsLWhlYWRpbmcnKTtcblxuICAgICAgICAgICAgICAgIGlmIChwYW5lbEhlYWRpbmcuaGFzQ2xhc3MoJ21pbicpKSB7XG4gICAgICAgICAgICAgICAgICAgIHBhbmVsSGVhZGluZy5yZW1vdmVDbGFzcygnbWluJyk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgcGFuZWxIZWFkaW5nLmFkZENsYXNzKCdtaW4nKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgc3RhdGljIGhhbmRsZUNvbGxlY3Rpb25zKCkge1xuICAgICAgICAkKGRvY3VtZW50KS5vbignY2xpY2snLCAnLnRvZ2dsZS1jb2xsZWN0aW9uJywgKHt0YXJnZXR9KSA9PiB7XG4gICAgICAgICAgICBjb25zdCBmbiA9ICQodGFyZ2V0KTtcblxuICAgICAgICAgICAgJCgnaW5wdXRbdHlwZT1jaGVja2JveF0uY29sbGVjdGlvbi1pdGVtJykuZWFjaCgoaSwgZSkgPT4ge1xuICAgICAgICAgICAgICAgICQoZSkucHJvcCgnY2hlY2tlZCcsIGZuLnByb3AoJ2NoZWNrZWQnKSk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgc3RhdGljIGhhbmRsZUJhdGNoQWN0aW9ucygpIHtcbiAgICAgICAgY29uc3Qgc2VsZWN0ZWQgPSAoKSA9PiB7XG4gICAgICAgICAgICByZXR1cm4gJCgnaW5wdXRbdHlwZT1jaGVja2JveF06Y2hlY2tlZC5jb2xsZWN0aW9uLWl0ZW0nKTtcbiAgICAgICAgfTtcblxuICAgICAgICAkKGRvY3VtZW50KS5vbignY2xpY2snLCAnLmJhdGNoLWFjdGlvbnMgYVtkYXRhLWFjdGlvbl0nLCAoe3RhcmdldH0pID0+IHtcbiAgICAgICAgICAgIGlmICghc2VsZWN0ZWQoKS5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGNvbnN0ICR0YXJnZXQgPSAkKHRhcmdldCk7XG5cbiAgICAgICAgICAgIGlmICgobXNnID0gJHRhcmdldC5kYXRhKCdjb25maXJtYXRpb24nKSkgJiYgIXdpbmRvdy5jb25maXJtKG1zZykpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICQoJyNiYXRjaF9hY3Rpb24nKS52YWwoJHRhcmdldC5kYXRhKCdhY3Rpb24nKSk7XG4gICAgICAgICAgICAkKCcjY29sbGVjdGlvbicpLnN1Ym1pdCgpO1xuXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIHN0YXRpYyBoYW5kbGVEYXRlQ29udHJvbHMoKSB7XG4gICAgICAgICQoJ1tkYXRhLWZpbHRlci10eXBlPVwiZGF0ZVwiXScpLmRhdGVwaWNrZXIoe1xuICAgICAgICAgICAgZm9ybWF0OiAneXl5eS1tbS1kZCcsXG4gICAgICAgICAgICBjbGVhckJ0bjogZmFsc2UsXG4gICAgICAgICAgICBtdWx0aWRhdGU6IGZhbHNlLFxuICAgICAgICB9KTtcblxuICAgICAgICAkKCdbZGF0YS1maWx0ZXItdHlwZT1cImRhdGVyYW5nZVwiXScpLmRhdGVyYW5nZXBpY2tlcih7XG4gICAgICAgICAgICBsb2NhbGU6IHtcbiAgICAgICAgICAgICAgICBmb3JtYXQ6ICdZWVlZLU1NLUREJyxcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBhdXRvVXBkYXRlSW5wdXQ6IGZhbHNlLFxuICAgICAgICAgICAgcmFuZ2VzOiB7XG4gICAgICAgICAgICAgICAgJ1RvZGF5JzogW21vbWVudCgpLCBtb21lbnQoKV0sXG4gICAgICAgICAgICAgICAgJ1llc3RlcmRheSc6IFtcbiAgICAgICAgICAgICAgICAgICAgbW9tZW50KCkuc3VidHJhY3QoMSwgJ2RheXMnKSxcbiAgICAgICAgICAgICAgICAgICAgbW9tZW50KCkuc3VidHJhY3QoMSwgJ2RheXMnKSxcbiAgICAgICAgICAgICAgICBdLFxuICAgICAgICAgICAgICAgICdMYXN0IDcgRGF5cyc6IFttb21lbnQoKS5zdWJ0cmFjdCg2LCAnZGF5cycpLCBtb21lbnQoKV0sXG4gICAgICAgICAgICAgICAgJ0xhc3QgMzAgRGF5cyc6IFttb21lbnQoKS5zdWJ0cmFjdCgyOSwgJ2RheXMnKSwgbW9tZW50KCldLFxuICAgICAgICAgICAgICAgICdUaGlzIE1vbnRoJzogW1xuICAgICAgICAgICAgICAgICAgICBtb21lbnQoKS5zdGFydE9mKCdtb250aCcpLFxuICAgICAgICAgICAgICAgICAgICBtb21lbnQoKS5lbmRPZignbW9udGgnKSxcbiAgICAgICAgICAgICAgICBdLFxuICAgICAgICAgICAgICAgICdMYXN0IE1vbnRoJzogW1xuICAgICAgICAgICAgICAgICAgICBtb21lbnQoKS5zdWJ0cmFjdCgxLCAnbW9udGgnKS5zdGFydE9mKCdtb250aCcpLFxuICAgICAgICAgICAgICAgICAgICBtb21lbnQoKS5zdWJ0cmFjdCgxLCAnbW9udGgnKS5lbmRPZignbW9udGgnKSxcbiAgICAgICAgICAgICAgICBdLFxuICAgICAgICAgICAgfSxcbiAgICAgICAgfSkub24oJ2FwcGx5LmRhdGVyYW5nZXBpY2tlcicsIGZ1bmN0aW9uKGV2LCBwaWNrZXIpIHtcbiAgICAgICAgICAgICQodGhpcykudmFsKHBpY2tlci5zdGFydERhdGUuZm9ybWF0KCdZWVlZLU1NLUREJykgKyAnIC0gJyArIHBpY2tlci5lbmREYXRlLmZvcm1hdCgnWVlZWS1NTS1ERCcpKTtcbiAgICAgICAgfSkub24oJ2NhbmNlbC5kYXRlcmFuZ2VwaWNrZXInLCBmdW5jdGlvbihldiwgcGlja2VyKSB7XG4gICAgICAgICAgICAkKHRoaXMpLnZhbCgnJyk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIHN0YXRpYyBoYW5kbGVMaXZlU2VhcmNoKCkge1xuICAgICAgICAkKCdbZGF0YS10eXBlPVwibGl2ZXNlYXJjaFwiXScpLnNlbGVjdGl6ZSh7XG4gICAgICAgICAgICB2YWx1ZUZpZWxkOiAnaWQnLFxuICAgICAgICAgICAgbGFiZWxGaWVsZDogJ25hbWUnLFxuICAgICAgICAgICAgc2VhcmNoRmllbGQ6IFsnbmFtZSddLFxuICAgICAgICAgICAgY3JlYXRlOiBmYWxzZSxcbiAgICAgICAgICAgIGxvYWRUaHJvdHRsZTogNTAwLFxuICAgICAgICAgICAgbWF4T3B0aW9uczogMTAwLFxuICAgICAgICAgICAgbG9hZDogZnVuY3Rpb24ocXVlcnksIGNhbGxiYWNrKSB7XG4gICAgICAgICAgICAgICAgaWYgKCFxdWVyeS5sZW5ndGggPj0gMykgcmV0dXJuIGNhbGxiYWNrKCk7XG5cbiAgICAgICAgICAgICAgICBsZXQgc2VsZWN0aXplID0gJCgkKHRoaXMpWzBdLiRpbnB1dCk7XG5cbiAgICAgICAgICAgICAgICBsZXQgYmFzZVVybCA9IHNlbGVjdGl6ZS5kYXRhKCd1cmwnKTtcbiAgICAgICAgICAgICAgICBsZXQgdXJsID0gYmFzZVVybCArICgtMSA9PT0gYmFzZVVybC5pbmRleE9mKCc/JykgPyAnPycgOiAnJicpO1xuICAgICAgICAgICAgICAgIHVybCArPSAncXVlcnk9JyArIHF1ZXJ5O1xuXG4gICAgICAgICAgICAgICAgJC5hamF4KHtcbiAgICAgICAgICAgICAgICAgICAgdXJsOiB1cmwsXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6ICdHRVQnLFxuICAgICAgICAgICAgICAgICAgICBlcnJvcjogY2FsbGJhY2ssXG4gICAgICAgICAgICAgICAgICAgIHN1Y2Nlc3M6IGZ1bmN0aW9uKHJlcykge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCFyZXMuaGFzT3duUHJvcGVydHkoJ2l0ZW1zJykpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnTGl2ZXNlYXJjaCByZXNwb25zZSBzaG91bGQgaGF2ZSBcIml0ZW1zXCIgY29sbGVjdGlvbi4gJyArXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICdFYWNoIGVsZW1lbnQgaW4gY29sbGVjdGlvbiBtdXN0IGhhdmUgYXQgbGVhc3QgMiBrZXlzOiBcImlkXCIgYW5kIFwibmFtZVwiJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICApO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gY2FsbGJhY2socmVzLml0ZW1zKTtcbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0sXG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIHN0YXRpYyBoYW5kbGVGYW5jeWJveCgpIHtcbiAgICAgICAgJCgnLmZhbmN5Ym94JykuZmFuY3lib3goe1xuICAgICAgICAgICAgYWZ0ZXJMb2FkOiBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICBsZXQgd2lkdGgsIGhlaWdodDtcbiAgICAgICAgICAgICAgICBpZiAod2lkdGggPSAkKHRoaXMuZWxlbWVudCkuZGF0YSgnd2lkdGgnKSkge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLndpZHRoID0gd2lkdGg7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgaWYgKGhlaWdodCA9ICQodGhpcy5lbGVtZW50KS5kYXRhKCdoZWlnaHQnKSkge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmhlaWdodCA9IGhlaWdodDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuICAgICAgICB9KTtcbiAgICB9XG59XG5cbiQoKCkgPT4gbmV3IEFkbWluQXJjaGl0ZWN0KTtcblxuZnVuY3Rpb24gdGVzdCgpIHtcbiAgICBhbGVydCgnYW0nKTtcbn1cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9yZXNvdXJjZXMvYXNzZXRzL2FkbWluaXN0cmF0b3IvanMvYXBwLmpzIl0sInNvdXJjZVJvb3QiOiIifQ==