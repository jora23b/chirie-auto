<?php

namespace App;

use Codesleeve\Stapler\ORM\EloquentTrait;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Illuminate\Database\Eloquent\Model;
use Terranet\Translatable\Translatable;
use Terranet\Translatable\HasTranslations;

class Slide extends Model implements Translatable, StaplerableInterface
{
    use HasTranslations, EloquentTrait {
        HasTranslations::getAttribute as getTranslatedAttribute;
        HasTranslations::setAttribute as setTranslatedAttribute;
        EloquentTrait::getAttribute as getStaplerableAttribute;
        EloquentTrait::setAttribute as setStaplerableAttribute;
    }

    protected $fillable = ['rank', 'image', 'active'];

    protected $translatedAttributes = ['title', 'description', 'link'];

    public function __construct(array $attributes = array())
    {
        $this->hasAttachedFile('image', [
            'styles' => $this->getStylesAttached()
        ]);

        parent::__construct($attributes);
    }

    public function getStylesAttached($hash = '#')
    {
        return [
            'medium' => '300x300' . $hash,
            'thumb' => '100x100' . $hash,
        ];
    }

    public function getAttribute($key)
    {
        if ($this->isKeyReturningTranslationText($key)) {
            if (!$translate = $this->getTranslatedAttribute($key)) {
                if ($translate = $this->translate(\localizer\getDefault()->id())) {
                    return $translate->$key;
                }
                return null;
            }
            return $translate;
        } else {
            if (array_key_exists($key, $this->attachedFiles)) {
                return $this->getStaplerableAttribute($key);
            }
        }
        return parent::getAttribute($key);
    }

    public function setAttribute($key, $value)
    {
        if ($this->hasTranslatedAttributes() && in_array($key, $this->translatedAttributes)) {
            return $this->setTranslatedAttribute($key, $value);
        } else {
            if (array_key_exists($key, $this->attachedFiles)) {
                return $this->setStaplerableAttribute($key, $value);
            }
        }
        return parent::setAttribute($key, $value);
    }
}
