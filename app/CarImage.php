<?php

namespace App;

use Codesleeve\Stapler\ORM\EloquentTrait;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Illuminate\Database\Eloquent\Model;

class CarImage extends Model implements StaplerableInterface
{
    use EloquentTrait;

    protected $fillable = ['image'];

    public $timestamps = false;

    public function __construct(array $attributes = array()) {
        $this->hasAttachedFile('image', [
            'styles' => [
                'medium' => '300x300#',
                'thumb' => '100x100#'
            ]
        ]);

        parent::__construct($attributes);
    }

    public static function getImagesByCarId($id)
    {
        return static::where('car_id', $id)->get();
    }
}
