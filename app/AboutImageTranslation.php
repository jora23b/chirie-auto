<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AboutImageTranslation extends Model
{
    protected $fillable = ['name'];

    public $timestamps = false;
}
