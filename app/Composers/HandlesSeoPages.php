<?php
namespace App\Composers;

use App\Car;
use App\Page;
use Artesaos\SEOTools\Contracts\SEOTools as SEOToolsContract;
use Artesaos\SEOTools\Traits\SEOTools;
use Illuminate\Database\Eloquent\Model;

trait HandlesSeoPages
{
    use SEOTools;

    /**
     * Retrieve the Seo Object by parameter name.
     *
     * @param string $type
     *
     * @return Model
     */
    protected function seoObject($type)
    {
        switch ($type) {
            case 'page':
            case 'car':
                return $this->route->parameter($type);
        }
        return null;
    }

    /**
     * Set Car SEO tags.
     *
     * @param Car $car
     * @return SEOToolsContract
     */
    protected function carMeta(Car $car)
    {
        $meta = $this->genericMeta($car);
        $meta->opengraph()->setUrl($this->currentUrl());
        if (file_exists(public_path($image = $car->image->url('detail')))) {
            $meta->opengraph()->addImage(url($image));
        }

        return $meta;
    }

    /**
     * Set Page SEO tags.
     *
     * @param Page $page
     * @return SEOToolsContract
     */
    protected function pageMeta(Page $page)
    {
        return $this->genericMeta($page);
    }

    /**
     * Set generic SEO tags for eloquent object.
     *
     * @param $eloquent - a model having meta.
     * @return SEOToolsContract
     */
    protected function genericMeta($eloquent)
    {
        $descriptionKey = $this->descriptionKey($eloquent);
        with($seo = $this->seo())
            ->setTitle(!empty($eloquent->meta_title) ? $eloquent->meta_title : $eloquent->title)
            ->setDescription(!empty($eloquent->meta_description) ? $eloquent->meta_description : $eloquent->$descriptionKey);
        $seo->metatags()->setKeywords(!empty($eloquent->meta_keywords) ? $eloquent->meta_keywords : $this->titleToKeywords($eloquent));
        $seo->opengraph()->addProperty('locale', $this->locale->locale());
        return $seo;
    }

    protected function descriptionKey($eloquent)
    {
        switch (get_class($eloquent)) {
            case Page::class:
                return 'body';
            default:
                return 'description';
        }
    }

    /**
     * @param $eloquent
     * @return mixed
     */
    protected function titleToKeywords($eloquent)
    {
        return str_replace(' ', ', ', $eloquent->title);
    }

    protected function routeMeta($name)
    {
        $this->seo()->setTitle(trans("meta.{$name}.title"));
        $this->seo()->setDescription(trans("meta.{$name}.description"));
        $this->seo()->metatags()->setKeywords(trans("meta.{$name}.keywords"));
    }
}