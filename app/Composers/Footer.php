<?php
/**
 * Created by PhpStorm.
 * User: jora
 * Date: 03.08.2017
 * Time: 18:34
 */

namespace App\Composers;

use App\Services\MenuService;
use Illuminate\Contracts\View\View;

class Footer
{
    protected $menuService;

    public function __construct(MenuService $menuService)
    {
        $this->menuService = $menuService;
    }

    public function compose(View $view)
    {
        $view->menuService = $this->menuService->getMenuItemsByLocation('bottom');
    }
}