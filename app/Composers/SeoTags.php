<?php
namespace App\Composers;

use App\Car;
use App\Page;

use App\Services\PageService;
use Illuminate\Routing\Route;
use Illuminate\Routing\Router;
use Illuminate\View\View;

class SeoTags
{
    use HandlesSeoPages;
    protected $locale;
    /**
     * @var Router
     */
    private $router;
    /**
     * @var Route
     */
    protected $route;

    public function __construct(Router $router)
    {
        $this->router = $router;
        $this->route = $router->current();
        $this->locale = \localizer\locale();
    }

    public function compose(View $view)
    {
        if (!$route = $this->route) {
            return false;
        }
        $routeName = $route->getName();

        switch ($routeName) {
            case 'page':
                return is_a($page = $this->seoObject('page'), Page::class)
                    ? $this->pageMeta($page)
                    : $this->routeMeta('page.404');
            case 'rent-car':
            case 'sale-car':
                return is_a($car = $this->seoObject('car'), Car::class)
                    ? $this->carMeta($car)
                    : $this->routeMeta('page.404');
            default:
                return $this->dynamicPage($routeName);
        }
    }

    protected function dynamicPage($key)
    {
        $page = app(PageService::class)->getPageByKey($key);

        if ($page) {
            return $this->pageMeta($page);
        }

        return $this->routeMeta('page.404');
    }

    protected function currentUrl()
    {
        return \URL::current();
    }
}