<?php
namespace App\Providers;

use App\Composers\Footer;
use App\Composers\Header;
use App\Composers\SeoTags;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
class ComposerServiceProvider extends ServiceProvider
{
    protected $composers = [
        SeoTags::class => ['layouts.partials.seo'],
        Footer::class => ['layouts.partials.footer'],
        Header::class => ['layouts.partials.header'],
    ];
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        foreach ($this->composers as $composer => $templates) {
            View::composer($templates, $composer);
        }
    }
    /**
     * Register any application services.
     */
    public function register()
    {
        //
    }
}