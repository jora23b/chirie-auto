<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarSpecification extends Model
{
    public $table = 'car_specification';

    protected $fillable = ['value'];

    public $timestamps = false;

    public function specification()
    {
        return $this->belongsTo('App\Specification');
    }

    public function car()
    {
        return $this->belongsTo('App\Car');
    }

    public static function getByCarId($carId)
    {
        $out = [];
        $items = static::where('car_id', $carId)->get();

        foreach ($items as $item) {
            $out[$item->specification_id][$item->language_id] = $item->value;
        }

        return $out;
    }

    public static function getSpecificationsByCarId($carId)
    {
        return static::where('car_id', $carId)->where('language_id', \localizer\locale()->id())
            ->where('value', '<>', '')->with('specification')->get();
    }
}
