<?php

namespace App;

use App\Presenters\CarPresenter;
use Codesleeve\Stapler\ORM\EloquentTrait;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Illuminate\Database\Eloquent\Model;
use Terranet\Presentable\PresentableInterface;
use Terranet\Presentable\PresentableTrait;
use Terranet\Translatable\HasTranslations;
use Terranet\Translatable\Translatable;

class Car extends Model implements Translatable, StaplerableInterface, PresentableInterface
{
    use HasTranslations, EloquentTrait {
        HasTranslations::getAttribute as getTranslatedAttribute;
        HasTranslations::setAttribute as setTranslatedAttribute;
        EloquentTrait::getAttribute as getStaplerableAttribute;
        EloquentTrait::setAttribute as setStaplerableAttribute;
    }
    use PresentableTrait;

    protected $presenter = CarPresenter::class;

    protected $fillable = [
        'type',
        'mark',
        'model',
        'price',
        'rank',
        'image',
        'year',
        'capacity',
        'fuel',
        'transmission',
        'mileage',
        'home_page',
        'active'
    ];

    protected $translatedAttributes = [
        'description',
        'color',
        'slug',
        'meta_title',
        'meta_description',
        'meta_keywords'
    ];

    const RENT = 'rent';
    const SALE = 'sale';

    protected static $types = [
        self::RENT,
        self::SALE,
    ];

    public static function availableFuels()
    {
        return array_merge(['' => '-'], trans('cars.specifications.fuels'));
    }

    public static function availableTransmission()
    {
        return array_merge(['' => '-'], trans('cars.specifications.transmissions'));
    }

    public static function availableTypes($empty = false)
    {
        $out = [];
        foreach (static::$types as $type) {
            $out[$type] = trans('administrator::columns.cars.types.' . $type);
        }

        if ($empty) {
            $out[''] = '- All -';
        }

        return $out;
    }

    public function __construct(array $attributes = array())
    {
        $this->hasAttachedFile('image', [
            'styles' => $this->getStylesAttached()
        ]);

        parent::__construct($attributes);
    }

    public function getStylesAttached($hash = '#')
    {
        return [
            'medium' => '300x300' . $hash,
            'thumb' => '100x100' . $hash,
        ];
    }

    public function images()
    {
        return $this->hasMany('App\CarImage');
    }

    public function specifications()
    {
        return $this->belongsToMany('App\Specification', 'car_specifications')->where('language_id',
            \localizer\locale()->id());
    }

    public function url()
    {
        if ($this->type == self::RENT) {
            return route('rent-car', $this->slug);
        }

        return route('sale-car', $this->slug);
    }

    public function getAttribute($key)
    {
        if ($this->isKeyReturningTranslationText($key)) {
            if (!$translate = $this->getTranslatedAttribute($key)) {
                if ($translate = $this->translate(\localizer\getDefault()->id())) {
                    return $translate->$key;
                }
                return null;
            }
            return $translate;
        } else {
            if (array_key_exists($key, $this->attachedFiles)) {
                return $this->getStaplerableAttribute($key);
            }
        }
        return parent::getAttribute($key);
    }

    public function setAttribute($key, $value)
    {
        if ($this->hasTranslatedAttributes() && in_array($key, $this->translatedAttributes)) {
            return $this->setTranslatedAttribute($key, $value);
        } else {
            if (array_key_exists($key, $this->attachedFiles)) {
                return $this->setStaplerableAttribute($key, $value);
            }
        }
        return parent::setAttribute($key, $value);
    }
}
