<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SlideTranslation extends Model
{
    protected $fillable = ['title', 'description', 'link'];

    public $timestamps = false;
}
