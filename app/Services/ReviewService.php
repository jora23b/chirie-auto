<?php

namespace App\Services;

use App\Review;

class ReviewService
{
    protected $model;

    public function __construct(Review $model)
    {
        $this->model = $model;
    }

    public function getQuery()
    {
        return $this->model->where('active', 1);
    }

    public function getReviews()
    {
        return $this->getQuery()->orderBy('rank', 'ASC')->get();
    }
}