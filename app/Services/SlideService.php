<?php

namespace App\Services;

use App\Slide;

class SlideService
{
    protected $model;

    public function __construct(Slide $model)
    {
        $this->model = $model;
    }

    public function getQuery()
    {
        return $this->model->where('active', 1);
    }

    public function getSlides()
    {
        return $this->getQuery()->orderBy('rank', 'ASC')->get();
    }
}