<?php

namespace App\Services;

use App\AboutImage;

class AboutImageService
{
    protected $model;

    public function __construct(AboutImage $model)
    {
        $this->model = $model;
    }

    public function getQuery()
    {
        return $this->model->where('active', 1);
    }

    public function getImages()
    {
        return $this->getQuery()->orderBy('rank', 'ASC')->get();
    }
}