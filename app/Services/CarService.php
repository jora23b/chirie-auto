<?php

namespace App\Services;

use App\Car;

class CarService
{
    protected $model;

    public function __construct(Car $model)
    {
        $this->model = $model;
    }

    public function getQuery()
    {
        return $this->model->where('active', 1);
    }

    public function getCarsHomePageByType($type, $limit = 3)
    {
        return $this->getQuery()->where('type', $type)
            ->orderBy('home_page', 'DESC')
            ->orderBy('rank', 'ASC')
            ->limit($limit)
            //->with(['specifications'])
            ->get();
    }

    public function findBySlug($slug)
    {
        return $this->model->scopeTranslated($this->getQuery()->where('slug', $slug))->first();
    }

    public function getAllCars()
    {
        return $this->getQuery()->get();
    }
}