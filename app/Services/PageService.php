<?php

namespace App\Services;

use App\Page;

class PageService
{
    protected $model;

    public function __construct(Page $model)
    {
        $this->model = $model;
    }

    public function getQuery()
    {
        return $this->model->where('active', 1);
    }

    public function getPageByKey($key)
    {
        return $this->getQuery()->where('key', $key)->first();
    }

    public function findBySlug($slug)
    {
        return $this->model->scopeTranslated($this->getQuery()->where('slug', $slug))->first();
    }

    public function getAllPages()
    {
        return $this->getQuery()->get();
    }
}