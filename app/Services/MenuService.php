<?php

namespace App\Services;

use App\Menu;

class MenuService
{
    protected $model;

    public function __construct(Menu $model)
    {
        $this->model = $model;
    }

    public function getMenuItemsByLocation($location)
    {
        return $this->model->where('location', $location)
            ->where('active', 1)->with('page')->orderBy('rank')->get();
    }
}