<?php

namespace App\Listeners;

use App\Events\NewMessageEvent;
use Illuminate\Mail\Message;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class SendMessageToEmail implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  NewMessageEvent  $event
     * @return void
     */
    public function handle(NewMessageEvent $event)
    {
        if($messageModel = $event->message) {
            Mail::send(
                'emails.message', ['messageModel' => $messageModel], function (Message $message) {
                    $message->to(options_find('email_for_messages'), config('app.name'));
                    //$message->replyTo(config('mail.username'), config('app.name'));
                    $message->subject(config('app.name') . ' - Un Mesaj nou');
                }
            );
        }
    }
}
