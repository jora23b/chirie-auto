<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = ['name', 'phone', 'car_id', 'message'];

    public function car()
    {
        return $this->belongsTo('App\Car');
    }
}
