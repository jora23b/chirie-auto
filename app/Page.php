<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Terranet\Translatable\HasTranslations;
use Terranet\Translatable\Translatable;

class Page extends Model implements Translatable
{
    use HasTranslations;

    protected $fillable = ['active', 'key'];

    protected $translatedAttributes = ['slug', 'title', 'body', 'meta_title', 'meta_description', 'meta_keywords'];

    public static function getPagesList($empty = false)
    {
        $items = static::leftJoin('page_translations', function ($q) {
            $q->on('pages.id', '=', 'page_id');
            $q->where('language_id', \localizer\locale()->id());
        })
            ->orderBy('title', 'ASC')
            ->pluck('title', 'pages.id')->toArray();

        if ($empty) {
            $items[0] = '- All -';
        }

        return $items;
    }

    public function url()
    {
        return route('page', $this->slug);
    }
}
