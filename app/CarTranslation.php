<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class CarTranslation extends Model
{
    use Sluggable;

    protected $fillable = ['description', 'color', 'slug', 'meta_title', 'meta_description', 'meta_keywords'];

    public $timestamps = false;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => ['car.mark', 'car.model', 'color'],
            ]
        ];
    }

    public function car()
    {
        return $this->belongsTo('App\Car');
    }
}
