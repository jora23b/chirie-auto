<?php

namespace App;

use App\Presenters\MenuPresenter;
use Illuminate\Database\Eloquent\Model;
use Terranet\Presentable\PresentableInterface;
use Terranet\Presentable\PresentableTrait;
use Terranet\Translatable\HasTranslations;
use Terranet\Translatable\Translatable;

class Menu extends Model implements Translatable, PresentableInterface
{
    use HasTranslations, PresentableTrait;

    const HEADER = 'top';
    const FOOTER = 'bottom';

    protected static $locations = [
        self::HEADER,
        self::FOOTER,
    ];

    protected $fillable = ['page_id', 'location', 'rank', 'active'];

    protected $translatedAttributes = ['name'];

    protected $presenter = MenuPresenter::class;

    public static function availableLocation()
    {
        $out = [];
        foreach (static::$locations as $location) {
            $out[$location] = trans('administrator::columns.menu.locations.' . $location);
        }

        return $out;
    }

    public function page()
    {
        return $this->belongsTo('App\Page');
    }
}
