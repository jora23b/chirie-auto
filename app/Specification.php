<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Terranet\Translatable\HasTranslations;
use Terranet\Translatable\Translatable;

class Specification extends Model implements Translatable
{
    use HasTranslations;

    protected $fillable = ['rank'];

    protected $translatedAttributes = ['name'];

    public static function getSpecificationsList()
    {
        return static::leftJoin('specification_translations', function ($q) {
            $q->on('specifications.id', '=', 'specification_id');
            $q->where('language_id', \localizer\locale()->id());
        })
            ->orderBy('rank', 'ASC')
            ->pluck('name', 'specifications.id') ->toArray();
    }
}
