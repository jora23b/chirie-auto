<?php

namespace App\Presenters;

use Terranet\Presentable\Presenter;

class CarPresenter extends Presenter
{
    public function getTitle()
    {
        return $this->presentable->mark . ' ' . $this->presentable->model;
    }

    public function fuel()
    {
        return trans('cars.specifications.fuels.' . $this->presentable->fuel);
    }

    public function transmission()
    {
        return trans('cars.specifications.transmissions.' . $this->presentable->transmission);
    }
}