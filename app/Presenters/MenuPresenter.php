<?php

namespace App\Presenters;

use Terranet\Presentable\Presenter;

class MenuPresenter extends Presenter
{
    public function adminPageId()
    {
        return with($page = $this->presentable->page) ? $page->title : null;
    }
}