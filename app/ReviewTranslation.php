<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReviewTranslation extends Model
{
    protected $fillable = ['name', 'description'];

    public $timestamps = false;
}
