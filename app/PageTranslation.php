<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class PageTranslation extends Model
{
    use Sluggable;

    public $timestamps = false;

    protected $fillable = ['slug', 'title', 'body', 'meta_title', 'meta_description', 'meta_keywords'];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title',
                'onUpdate' => true
            ]
        ];
    }

    public function page()
    {
        return $this->belongsTo(Page::class);
    }
}
