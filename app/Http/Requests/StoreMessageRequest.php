<?php

namespace App\Http\Requests;

use App\Message;
use Illuminate\Foundation\Http\FormRequest;

class StoreMessageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'phone' => 'required',
            'message' => 'required',
            'email' => 'email',
        ];
    }

    public function save()
    {
        $message = new Message();
        $message->fill($this->only(['name', 'phone', 'message', 'car_id', 'email']));
        $message->save();

        return $message;
    }
}
