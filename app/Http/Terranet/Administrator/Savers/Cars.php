<?php

namespace App\Http\Terranet\Administrator\Savers;

use App\CarImage;
use App\CarSpecification;
use Terranet\Administrator\Services\Saver;

class Cars extends Saver
{
    protected $specifications = [];

    public function saveRelations()
    {
        $this->syncSpecifications();
        $this->saveImages();
        parent::saveRelations();
    }

    public function save()
    {
        unset($this->relations['administrator::columns.sections']);
        $this->parseTranslatable($this->request->get('translatable'));

        parent::save();
    }

    private function saveImages()
    {
        if ($this->request->file('images')) {
            foreach ($this->request->file('images') as $file) {
                $carImage = new CarImage();
                $carImage->car_id = $this->repository->id;
                $carImage->image->setUploadedFile($file);
                $carImage->save();
            }
        }
    }

    private function syncSpecifications()
    {
        CarSpecification::where('car_id', $this->repository->id)->delete();

        if ($this->specifications) {
            foreach ($this->specifications as $id => $items) {
                foreach ($items as $item) {
                    $item['car_id'] = $this->repository->id;
                    $item['specification_id'] = $id;
                    CarSpecification::create($item);
                }
            }
        }
    }

    private function parseTranslatable($translatable)
    {
        if ($translatable) {
            foreach ($translatable as $langId => $items) {
                unset($this->data[$langId]);
                foreach ($items as $key => $value) {
                    if (starts_with($key, 'specifications_')) {
                        $id = (int)explode('_', $key)[1];
                        $this->specifications[$id][] = ['language_id' => $langId, 'value' => $value];
                        continue;
                    }

                    $this->data[$langId][$key] = $value;
                }
            }
        }
    }
}