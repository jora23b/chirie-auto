<?php

namespace App\Http\Terranet\Administrator\Modules;

use Terranet\Administrator\Contracts\Module\Editable;
use Terranet\Administrator\Contracts\Module\Exportable;
use Terranet\Administrator\Contracts\Module\Filtrable;
use Terranet\Administrator\Contracts\Module\Navigable;
use Terranet\Administrator\Contracts\Module\Sortable;
use Terranet\Administrator\Contracts\Module\Validable;
use Terranet\Administrator\Form\Type\Tinymce;
use Terranet\Administrator\Scaffolding;
use Terranet\Administrator\Traits\Module\AllowFormats;
use Terranet\Administrator\Traits\Module\AllowsNavigation;
use Terranet\Administrator\Traits\Module\HasFilters;
use Terranet\Administrator\Traits\Module\HasForm;
use Terranet\Administrator\Traits\Module\HasSortable;
use Terranet\Administrator\Traits\Module\ValidatesForm;

/**
 * Administrator Resource Pages
 *
 * @package Terranet\Administrator
 */
class Pages extends Scaffolding implements Navigable, Filtrable, Editable, Validable, Sortable, Exportable
{
    use HasFilters, HasForm, HasSortable, ValidatesForm, AllowFormats, AllowsNavigation;

    /**
     * The module Eloquent model
     *
     * @var string
     */
    protected $model = '\App\Page';

    public function linkAttributes()
    {
        return ['icon' => 'fa fa-file-text'];
    }

    public function columns()
    {
        $columns = $this->scaffoldColumns();
        $columns->without(['key', 'slug', 'body', 'meta_title', 'meta_description', 'meta_keywords']);
        $columns->move('active', 'after:dates');

        return $columns;
    }

    public function form()
    {
        $form = $this->scaffoldForm();
        $form->without(['key', 'slug']);

        $form->update('body', function($element) {
            $element->setInput('tinymce')->setTranslatable(true);
        });
        $form->update('meta_description', function($element) {
            $element->setInput('textarea')->setTranslatable(true);
        });
        $form->update('meta_keywords', function($element) {
            $element->setInput('textarea')->setTranslatable(true);
        });

        return $form;
    }

    public function rules()
    {
        return array_merge($this->scaffoldRules(), [
            'translatable.*.title' => 'required',
        ]);
    }
}