<?php

namespace App\Http\Terranet\Administrator\Modules;

use Illuminate\Database\Eloquent\Model;
use Terranet\Administrator\Contracts\Module\Editable;
use Terranet\Administrator\Contracts\Module\Exportable;
use Terranet\Administrator\Contracts\Module\Filtrable;
use Terranet\Administrator\Contracts\Module\Navigable;
use Terranet\Administrator\Contracts\Module\Sortable;
use Terranet\Administrator\Contracts\Module\Validable;
use Terranet\Administrator\Scaffolding;
use Terranet\Administrator\Traits\Module\AllowFormats;
use Terranet\Administrator\Traits\Module\AllowsNavigation;
use Terranet\Administrator\Traits\Module\HasFilters;
use Terranet\Administrator\Traits\Module\HasForm;
use Terranet\Administrator\Traits\Module\HasSortable;
use Terranet\Administrator\Traits\Module\ValidatesForm;

/**
 * Administrator Resource Message
 *
 * @package Terranet\Administrator
 */
class Message extends Scaffolding implements Navigable, Filtrable, Editable, Validable, Sortable, Exportable
{
    use HasFilters, HasForm, HasSortable, ValidatesForm, AllowFormats, AllowsNavigation;

    /**
     * The module Eloquent model
     *
     * @var string
     */
    protected $model = '\App\Message';

    public function linkAttributes()
    {
        return ['icon' => 'fa fa-envelope'];
    }

    public function columns()
    {
        $columns = $this->scaffoldColumns();
        $columns->without(['message', 'car_id']);

        return $columns;
    }

    public function viewColumns(Model $model = null)
    {
        $columns = $this->scaffoldViewColumns($model);
        $columns->update('car_id', function ($element) {
            $element->setTemplate(function ($row) {
                if($row->car_id) {
                    return '<a href="' . $row->car->url() . '">' . $row->car->getPresenter()->getTitle() . '</a>';
                }

                return null;
            });
        });

        return $columns;
    }

    public function canCreate()
    {
        return false;
    }

    public function canUpdate()
    {
        return false;
    }
}