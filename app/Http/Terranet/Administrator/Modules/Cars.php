<?php

namespace App\Http\Terranet\Administrator\Modules;

use App\Car;
use App\CarImage;
use App\CarSpecification;
use App\Specification;
use Illuminate\Database\Eloquent\Model;
use Terranet\Administrator\Columns\Element;
use Terranet\Administrator\Contracts\Module\Editable;
use Terranet\Administrator\Contracts\Module\Exportable;
use Terranet\Administrator\Contracts\Module\Filtrable;
use Terranet\Administrator\Contracts\Module\Navigable;
use Terranet\Administrator\Contracts\Module\Sortable;
use Terranet\Administrator\Contracts\Module\Validable;
use Terranet\Administrator\Filters\FilterElement;
use Terranet\Administrator\Form\Collection\Mutable;
use Terranet\Administrator\Form\FormElement;
use Terranet\Administrator\Form\FormSection;
use Terranet\Administrator\Form\Type\Select;
use Terranet\Administrator\Scaffolding;
use Terranet\Administrator\Traits\Module\AllowFormats;
use Terranet\Administrator\Traits\Module\AllowsNavigation;
use Terranet\Administrator\Traits\Module\HasFilters;
use Terranet\Administrator\Traits\Module\HasForm;
use Terranet\Administrator\Traits\Module\HasSortable;
use Terranet\Administrator\Traits\Module\ValidatesForm;

/**
 * Administrator Resource Cars
 *
 * @package Terranet\Administrator
 */
class Cars extends Scaffolding implements Navigable, Filtrable, Editable, Validable, Sortable, Exportable
{
    use HasFilters, HasForm, HasSortable, ValidatesForm, AllowFormats, AllowsNavigation;

    /**
     * The module Eloquent model
     *
     * @var string
     */
    protected $model = '\App\Car';

    public function linkAttributes()
    {
        return ['icon' => 'fa fa-car'];
    }

    public function columns()
    {
        $columns = $this->scaffoldColumns();
        $columns->without([
            'meta_title',
            'meta_description',
            'meta_keywords',
            'description',
            'slug',
            'color',
            'fuel',
            'transmission',
            'mileage',
            'year',
            'capacity',
            'home_page',
        ]);
        $columns->move('image', 'after:type');

        return $columns;
    }

    public function form()
    {
        $form = $this->scaffoldForm();
        $form->without(['slug']);

        $form->update('description', function ($element) {
            $element->setInput('tinymce')->setTranslatable(true);
        });
        $form->update('meta_description', function ($element) {
            $element->setInput('textarea')->setTranslatable(true);
        });
        $form->update('meta_keywords', function ($element) {
            $element->setInput('textarea')->setTranslatable(true);
        });
        $form->update('type', function ($element) {
            $element->setInput(new Select('type'));
            $element->getInput()->setOptions(Car::availableTypes());

            return $element;
        });
        $form->update('fuel', function ($element) {
            $element->setInput(new Select('fuel'));
            $element->getInput()->setOptions(Car::availableFuels());

            return $element;
        });
        $form->update('transmission', function ($element) {
            $element->setInput(new Select('transmission'));
            $element->getInput()->setOptions(Car::availableTransmission());

            return $element;
        });

        if($eloquent = app('scaffold.model')) {
            $eloquent->hasAttachedFile('image', [
                'styles' => $eloquent->getStylesAttached('')
            ]);
        }

        $form->insert(new FormSection(trans('administrator::columns.sections.general')), 0);
        $form->move('mark', 1);
        $form->move('model', 'after:mark');

        $form->move('meta_title', 'after:active');
        $form->insert(new FormSection(trans('administrator::columns.sections.meta')), 'after:active');
        $form->move('meta_description', 'after:meta_title');
        $form->move('meta_keywords', 'after:meta_description');

        $form = $this->addSpecificationsColumns($form);
        $form = $this->addImages($form);

        return $form;
    }

    private function addImages(Mutable $form)
    {
        $form->push(new FormSection(trans('administrator::columns.sections.images')));
        $input = FormElement::image('images');

        $images = $this->routeParam('id') ? CarImage::getImagesByCarId($this->routeParam('id')) : null;

        $input->setView('administrator::cars.images', compact('images'));
        $form->push($input);

        return $form;
    }

    private function addSpecificationsColumns(Mutable $form)
    {
        $specifications = Specification::getSpecificationsList();

        if ($specifications > 0) {
            $form->push(new FormSection(trans('administrator::columns.sections.specifications')));

            $carSpecifications = $this->routeParam('id') ? CarSpecification::getByCarId($this->routeParam('id')) : null;

            foreach ($specifications as $id => $name) {
                $input = FormElement::text($name);
                $values = isset($carSpecifications[$id]) ? $carSpecifications[$id] : null;

                $input->setView('administrator::cars.specifications', compact('values', 'id'));
                $form->push($input);
            }
        }

        return $form;
    }

    private function viewSpecifications(Mutable $columns)
    {
        $carSpecifications = CarSpecification::getSpecificationsByCarId($this->routeParam('id'));

        foreach ($carSpecifications as $specification) {
            $column = new Element($specification->specification->name);
            $column->setTemplate($specification->value);
            $columns->push($column);
        }

        return $columns;
    }

    public function viewColumns(Model $model = null)
    {
        $columns = $this->scaffoldViewColumns($model);
        $columns = $this->viewSpecifications($columns);

        $column = new Element('images');
        $column->setTemplate(function ($row) {
            $template = view('administrator::cars.view.images', compact('row'));

            return $template->render();
        });
        $columns->push($column);

        return $columns;
    }

    public function filters()
    {
        $filters = $this->scaffoldFilters();

        $filters->push(FilterElement::select('type', [], Car::availableTypes(true)));

        return $filters;
    }

    public function rules()
    {
        return array_merge($this->scaffoldRules(), [
            'model' => 'required',
            'mark' => 'required',
            'type' => 'required',
            'price' => '',
            'slug' => '',
            'rank' => '',
            'year' => '',
            'capacity' => '',
            'mileage' => '',
        ]);
    }
}