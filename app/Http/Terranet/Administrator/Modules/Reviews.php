<?php

namespace App\Http\Terranet\Administrator\Modules;

use Terranet\Administrator\Contracts\Module\Editable;
use Terranet\Administrator\Contracts\Module\Exportable;
use Terranet\Administrator\Contracts\Module\Filtrable;
use Terranet\Administrator\Contracts\Module\Navigable;
use Terranet\Administrator\Contracts\Module\Sortable;
use Terranet\Administrator\Contracts\Module\Validable;
use Terranet\Administrator\Form\FormElement;
use Terranet\Administrator\Scaffolding;
use Terranet\Administrator\Traits\Module\AllowFormats;
use Terranet\Administrator\Traits\Module\AllowsNavigation;
use Terranet\Administrator\Traits\Module\HasFilters;
use Terranet\Administrator\Traits\Module\HasForm;
use Terranet\Administrator\Traits\Module\HasSortable;
use Terranet\Administrator\Traits\Module\ValidatesForm;

/**
 * Administrator Resource Slides
 *
 * @package Terranet\Administrator
 */
class Reviews extends Scaffolding implements Navigable, Filtrable, Editable, Validable, Sortable, Exportable
{
    use HasFilters, HasForm, HasSortable, ValidatesForm, AllowFormats, AllowsNavigation;

    /**
     * The module Eloquent model
     *
     * @var string
     */
    protected $model = '\App\Review';

    public function linkAttributes()
    {
        return ['icon' => 'fa fa-id-card'];
    }

    public function columns()
    {
        $columns = $this->scaffoldColumns();
        $columns->without(['description']);
        $columns->move('name', 'after:id');

        return $columns;
    }

    public function form()
    {
        $form = $this->scaffoldForm();
//        $form->update('description', function ($element) {
//            $element->setInput('textarea')->setTranslatable(true);
//        });

        if($eloquent = app('scaffold.model')) {
            $eloquent->hasAttachedFile('image', [
                'styles' => $eloquent->getStylesAttached('')
            ]);
        }

        return $form;
    }

    public function rules()
    {
        return array_merge($this->scaffoldRules(), [
            'translatable.*.name' => 'required',
            'rank' => '',
        ]);
    }
}