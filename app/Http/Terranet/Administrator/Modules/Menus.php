<?php

namespace App\Http\Terranet\Administrator\Modules;

use App\Menu;
use App\Page;
use Illuminate\Database\Eloquent\Model;
use Terranet\Administrator\Contracts\Module\Editable;
use Terranet\Administrator\Contracts\Module\Exportable;
use Terranet\Administrator\Contracts\Module\Filtrable;
use Terranet\Administrator\Contracts\Module\Navigable;
use Terranet\Administrator\Contracts\Module\Sortable;
use Terranet\Administrator\Contracts\Module\Validable;
use Terranet\Administrator\Form\FormElement;
use Terranet\Administrator\Form\Type\Select;
use Terranet\Administrator\Scaffolding;
use Terranet\Administrator\Traits\Module\AllowFormats;
use Terranet\Administrator\Traits\Module\AllowsNavigation;
use Terranet\Administrator\Traits\Module\HasFilters;
use Terranet\Administrator\Traits\Module\HasForm;
use Terranet\Administrator\Traits\Module\HasSortable;
use Terranet\Administrator\Traits\Module\ValidatesForm;

/**
 * Administrator Resource Menus
 *
 * @package Terranet\Administrator
 */
class Menus extends Scaffolding implements Navigable, Filtrable, Editable, Validable, Sortable, Exportable
{
    use HasFilters, HasForm, HasSortable, ValidatesForm, AllowFormats, AllowsNavigation;

    /**
     * The module Eloquent model
     *
     * @var string
     */
    protected $model = '\App\Menu';

    public function linkAttributes()
    {
        return ['icon' => 'fa fa-bars'];
    }

    public function columns()
    {
        $columns = $this->scaffoldColumns();

        $columns->move('name', 'after:id');

        return $columns;
    }

    public function form()
    {
        $form = $this->scaffoldForm();

        $form->update('location', function ($element) {
            $element->setInput(new Select('location'));
            $element->getInput()->setOptions(Menu::availableLocation());

            return $element;
        });

        $form->update('page_id', function ($element) {
            $element->setInput(new Select('page_id'));
            $element->getInput()->setOptions(Page::getPagesList());

            return $element;
        });

        return $form;
    }

    public function rules()
    {
        return array_merge($this->scaffoldRules(), [
            'translatable.*.name' => 'required',
            'rank' => '',
        ]);
    }

    public function viewColumns(Model $model = null)
    {
        $columns = $this->scaffoldViewColumns($model);
        $columns->move('name', 'after:id');

        return $columns;
    }
}