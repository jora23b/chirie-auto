<?php

namespace App\Http\Terranet\Administrator\Actions;

use Illuminate\Database\Eloquent\Model;
use Terranet\Administrator\Services\CrudActions;

class Pages extends CrudActions
{
    public function delete(Model $item)
    {
        if ($item->key) {

            return back()->withErrors(trans('administrator::errors.permission_delete_page'));
        }

        return parent::delete($item);
    }
}