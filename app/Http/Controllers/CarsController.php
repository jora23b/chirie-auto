<?php

namespace App\Http\Controllers;

use App\Car;
use App\Services\CarService;
use Illuminate\Http\Request;

class CarsController extends Controller
{
    protected $type;
    protected $service;

    public function __construct(CarService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        return view('cars.index');
    }

    public function view(Car $car)
    {
        return view('cars.view');
    }
}
