<?php

namespace App\Http\Controllers;

use App\Services\CarService;
use App\Services\PageService;
use Codesleeve\Stapler\Interfaces\Config;
use Illuminate\Http\Request;

class SiteMapController extends Controller
{
    protected $urls;

    public function index(PageService $pageService, CarService $carService)
    {
        $pages = $pageService->getAllPages();
        $cars = $carService->getAllCars();

        $urls = [];
        foreach (\localizer\locales() as $locale) {
            //trans('key', [], 'en');
        }

    }

    private function pagesUrls($pages, $locale)
    {
        foreach ($pages as $page) {

        }
    }

    private function carsUrls($cars, $locale)
    {

    }

    private function getUrl($url, $locale)
    {
        return \localizer\url($locale->iso6391());
    }
}
