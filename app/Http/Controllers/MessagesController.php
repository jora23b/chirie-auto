<?php

namespace App\Http\Controllers;

use App\Events\NewMessageEvent;
use App\Http\Requests\StoreMessageRequest;
use Illuminate\Http\Request;

class MessagesController extends Controller
{
    public function store(StoreMessageRequest $request)
    {
        $message = $request->save();

        \Event::fire(new NewMessageEvent($message));

        return back()->with(trans('messages.message_success_send'));
    }
}
