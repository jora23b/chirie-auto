<?php

namespace App\Http\Controllers;

use App\Services\AboutImageService;
use Illuminate\Http\Request;

class AboutUsController extends Controller
{
    public function index(AboutImageService $aboutImageService)
    {
        $images = $aboutImageService->getImages();

        return view('about-us', compact('images'));
    }
}
