<?php

namespace App\Http\Controllers;

use App\Car;
use App\Services\CarService;
use App\Services\ReviewService;
use App\Services\SlideService;
use Illuminate\Http\Request;

class HomePageController extends Controller
{
    public function index(CarService $carService, SlideService $slideService, ReviewService $reviewService)
    {
        $rentCars = $carService->getCarsHomePageByType(Car::RENT);
        $saleCars = $carService->getCarsHomePageByType(Car::SALE, 1);

        $slides = $slideService->getSlides();

        $reviews = $reviewService->getReviews();

        return view('home-page', compact('rentCars', 'saleCars', 'slides', 'reviews'));
    }
}
